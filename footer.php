<div class="svg-footer">
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="none" width="100%" height="10%">
  <path d="M0 100 L100 100 L100 2 L0 100 Z" stroke-width="0"></path>
</svg>
</div>
  <footer class="site-footer">
    <div class="container container--narrow">
      <div class="group">
        <div class="site-footer__col-one">
          <nav class="nav-list">
            <?php wp_nav_menu(array(
              'theme_location' => 'footer_1_Menu',
            )); ?>
          </nav>
        </div>

        <div class="site-footer__col-two">
            <nav class="nav-list">
              <?php wp_nav_menu(array(
                'theme_location' => 'footer_2_Menu',
              )); ?>
            </nav>
          </div>

          <div class="site-footer__col-three">
		<nav class="nav-list">
			<?php wp_nav_menu(array(
				'theme_location' => 'footer_3_Menu',
				)); ?>
		</nav>
          </div>

        <div class="site-footer__col-four">
          <h3 class="headline headline--small">Connect With Us</h3>
          <nav>
            <ul class="min-list social-icons-list group">
              <li><a href="https://www.facebook.com/kentwingaircadets" class="social-color-facebook"><i class="fab fa-facebook-square" aria-hidden="true"></i></a></li>
              <li><a href="https://twitter.com/kentaircadets" class="social-color-twitter"><i class="fab fa-twitter-square" aria-hidden="true"></i></a></li>
<!--          <li><a href="#" class="social-color-youtube"><i class="fab fa-youtube-square" aria-hidden="true"></i></a></li> -->
              <li><a href="https://www.instagram.com/kentaircadets" class="social-color-instagram"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
          </nav>
        </div>
      </div>
	<p style="text-aligh:center;color:white;">Crown Copyright © 2020</p>
    </div>
  </footer>

<?php wp_footer(); ?>

</body>
</html>
