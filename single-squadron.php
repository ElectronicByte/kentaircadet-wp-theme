<?php

get_header();

while(have_posts()) {
the_post();

?>

<div class="page-banner">
  <div class="page-banner__bg-image" style="background-image: url(<?php $banner_bg_image = get_field('banner_bg_image'); echo $banner_bg_image['sizes']['Pagebanner']; ?>);"></div>
  <div class="page-banner__social-banner">
	<?php 	$SocialArray=array('tel','squadron_email','squadron_website','public_fb_page','public_twitter','public_instagram');
                foreach($SocialArray as $value){
                  $sqnweb = get_field($value);
                  if(!empty($sqnweb)){
                        if ($value =='squadron_website'){
                                ?>
                                <a href="<?php echo $sqnweb ?>"><i class="fas fa-globe-europe fa-2x fawhite" aria-hidden="true"></i></a>
                                <?php }
                        elseif ($value == 'public_fb_page') {
                                ?>
                                <a href="<?php echo $sqnweb ?>"><i class="fab fa-facebook-square fa-2x fawhite" aria-hidden="true"></i></a>
                                <?php }
                        elseif ($value == 'public_twitter') {
                                ?>
                                <a href="<?php echo $sqnweb ?>"><i class="fab fa-twitter-square fa-2x fawhite" aria-hidden="true"></i></a>
                                <?php }
                        elseif ($value == 'public_instagram') {
                                ?>
                                <a href="<?php echo $sqnweb ?>"><i class="fab fa-instagram fa-2x fawhite" aria-hidden="true"></i></a>
                                <?php }
                        elseif ($value == 'squadron_email'){
                                ?>
                                <a href="mailto:<?php echo $sqnweb ?>"><i class="fas fa-at fa-2x fawhite" aria-hidden="true"></i></a>
                                <?php }

                      }
                }

  	?>
  </div>
  <div class="page-banner__content container container--narrow">
  
    <h1 class="page-banner__title"><?php echo the_title(); ?></h1>
    <div class="page-banner__intro">
      <p><?php the_field('banner_subtitle') ?></p>
    </div>
  </div>
</div>

<div class="container container--narrow page-section">
  <div class="metabox metabox--position-up metabox--with-home-link">
    <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('squadron'); ?>">Squadrons</a> <span class="metabox__main"><?php the_title(); ?></span></p>
  </div>

  <div class="generic-content">
    <?php the_content(); ?>
  </div>
</div>
<?php
}
get_footer();

?>
