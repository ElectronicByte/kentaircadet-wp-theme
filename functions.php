<?php

function style_files(){ /* Function to load in styling files (css, icon packs, JS etc) */
	wp_enqueue_script('theme_JS', get_theme_file_uri('/js/scripts-bundled.js'), NULL, microtime(), true); /* Change microtime() to version number prior to release */
	wp_enqueue_script('theme_JS', get_theme_file_uri('/js/modules/animatedmenu.js'), NULL, microtime(), true);
	wp_enqueue_style('Google_Roboto_Font', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
	wp_enqueue_style('font_awesome_icons', 'https://use.fontawesome.com/releases/v5.12.0/css/all.css');
	wp_enqueue_style('Leaflet Maps CSS', 'https://unpkg.com/leaflet@1.6.0/dist/leaflet.css');
	wp_enqueue_style('main_styles', get_stylesheet_uri(), NULL, microtime());
	wp_enqueue_script('Leaflet Base JS', 'https://unpkg.com/leaflet@1.6.0/dist/leaflet.js');
}
add_action('wp_enqueue_scripts','style_files');

function setup_tasks(){ /* Function to add features to the site */

	add_theme_support('title-tag'); // Add title that shows in web browser tab
	register_nav_menu('headerMenu', 'Header Menu');
	register_nav_menu('footer_1_Menu', 'Footer One');
	register_nav_menu('footer_2_Menu', 'Footer Two');
	register_nav_menu('footer_3_Menu', 'Footer Three');
	add_theme_support( 'align-wide' );
	add_theme_support( 'post-thumbnails' );
	add_image_size('Pagebanner', 1500, 350, true);
}
add_action('after_setup_theme','setup_tasks');

function add_opengraph($output){ /* function adds ingormation about OoenGraph tobthe head */
	return $output . '
	xmlns="https://www.w3.org/1999/xhtml"
    	xmlns:og="https://ogp.me/ns#" 
    	xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph');
	
function excerptLength($length){
	return 30;
}
add_filter('excerpt_length', 'excerptLength');

function PostTypes(){
	register_post_type('Squadron',array(
		'public' => true,
		'show_in_rest' => true,
		'supports' => array('title', 'author', 'thumbnail', 'editor'),
		'rewrite' => array(
			'slug' => 'squadrons',
		),
		'has_archive' => true,
		'show_in_admin_bar' => false,
		'labels' => array(
			'name' => 'Squadrons',
			'singular_name' => 'Squadron',
			'add_new' => 'Create New',
		 	'add_new_item' => 'Add new Squadron Page',
			'new_item' => 'New Squadron',
			'item_published' => 'Squadron Page Pubkished',
			'edit_item' => 'Edit Squadron Page',
			'view_item' => 'View Squadron Page',
			'view_items' => 'View Squadrons Map',
			'item_updated' => 'Squadron Page Updated',
			'all_items' => 'All Squadrons',
			'singular_item' => 'Squadron'),
			'menu_icon' => 'dashicons-location',
			'search_items' => 'Search Squadrons',
			'not_found' => 'No Squadron Found',
			'not_found_in_trash' => 'No Archived Squadron',
			'parent_item_colon' => 'Parent Squadron',
			'archives' => 'Squadrons',
			'attributes' => '',
	));
}
add_action('init', 'PostTypes');

function custom_type_archive_display($query) {
    if (is_post_type_archive('squadron')) {
         $query->set('posts_per_page',40);
        return;
    }
}
add_action('pre_get_posts', 'custom_type_archive_display');


// Removes from admin menu
add_action( 'admin_menu', 'remove_admin_menus' );
function remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'admin_bar_render' );

//Remove default user roles
add_action('admin_menu', 'remove_built_in_roles');
 
function remove_built_in_roles() {
    global $wp_roles;
 
    $roles_to_remove = array('subscriber', 'contributor', 'author', 'editor');
 
    foreach ($roles_to_remove as $role) {
        if (isset($wp_roles->roles[$role])) {
            $wp_roles->remove_role($role);
        }
    }
}

function wp_login_logo() {

$randomNumber = rand(1,11); 
?>
    <style type="text/css">
	#login h1 {
 		text-align: center;
    		background-color: #fff;
		border: 1px solid #ccd0d4;
		box-shadow: 0 1px 3px rgba(0,0,0,.04);
	}
        #login h1 a, .login h1 a {
                background-image: url(<?php echo get_theme_file_uri('/images/RAFAC-kent-login.png'); ?>);
                height:100px;
                width:300px;
                background-size: 325px 95px;
                background-repeat: no-repeat;
                padding-bottom: 10px;
        }
        body.login {
                background-image: url('<?php echo "https://cadets.bader.mod.uk/img/RAFAC/LoginScreenImages/RAFAC_HERO".$randomNumber.".jpg" ?>');
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center;
		background-size: cover;
        }
    </style>
<?php }
add_action('login_enqueue_scripts', 'wp_login_logo' );



function wp_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'wp_login_logo_url' );
 
function wp_login_logo_url_title() {
    return 'Kent Air Cadets';
}
add_filter( 'login_headertext', 'wp_login_logo_url_title' );

?>
