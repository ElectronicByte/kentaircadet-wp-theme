<?php

get_header();

while(have_posts()) {
the_post();
?>

<meta property="og:image" content="<?php echo get_the_post_thumbnail_url(get_the_ID(),'full');?>" /> 
<div class="page-banner">
  <div class="page-banner__bg-image" style="background-image: url(<?php
	if(get_field('banner_bg_image')) {
      		$banner_bg = get_field('banner_bg_image');
		echo $banner_bg['sizes']['Pagebanner'];
	} else {
		$banner_bg = "https://kentaircadets.com/wp-content/uploads/2020/01/DSC00530-scaled-e1578657936701.jpg";
		echo $banner_bg;
	}
      ?>
      );">
  </div>
  <div class="page-banner__content container container--narrow">
    <h1 class="page-banner__title"><?php the_title(); ?></h1>
    <div class="page-banner__intro">
      <p><?php the_excerpt(); ?>
  </div>
</div>
</div>
<div class="container container--narrow page-section">
  <div class="metabox metabox--position-up metabox--with-home-link">
    <p><a class="metabox__blog-home-link" href="<?php echo site_url('/news') ?>"><i class="far fa-newspaper" aria-hidden="true"></i> News</a> <span class="metabox__main">Posted by <?php the_author_posts_link(); ?> on <?php the_time('j F, Y'); ?> in <?php echo get_the_category_list(', '); ?></span></p>
  </div>

  <div class="generic-content">
    <?php the_content(); ?>
  </div>
</div>
<?php
}
get_footer();

?>
