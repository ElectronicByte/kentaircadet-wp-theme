<?php

get_header();
?>

<div class="page-banner">
  <div class="page-banner__bg-image" style="background-image: url(<?php
      $banner_bg = get_field('banner_bg_image', get_option( 'page_for_posts'));
      echo $banner_bg['sizes']['Pagebanner'];
      ?>
      );">
  </div>
  <div class="page-banner__content container container--narrow">
    <h1 class="page-banner__title"><?php echo single_post_title(); ?></h1>
    <div class="page-banner__intro">
      <p><?php the_field('banner_subtitle', get_option( 'page_for_posts'));?></p>
    </div>
  </div>
</div>

<div class="container container--narrow page-section">
  <?php
while(have_posts()){
    	the_post(); ?>
      	<div class="post-item">
        	<h2><a class="headline headline--medium headline--post-title" href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h2>
        	<div class="metabox">
          		<p>Posted by <?php the_author_posts_link(); ?> on <?php the_time('j F, Y'); ?> in <?php echo get_the_category_list(', '); ?></p>
        	</div>
        	<div class="generic-content">
			<?php the_excerpt(); ?> 
		</div>
		<a href="<?php the_permalink(); ?>"> Read more . . .</a>
        	<?php if(has_post_thumbnail()){ 
			$imageurl = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "size" );
			if ($imageurl[1] > $imageurl[2]) {
				?> 
				<div class="feat-image" style="">
				<?php
				the_post_thumbnail('large', ['class' => 'feat-image__bg-image_land', 'title' => 'Feature image']);
			} elseif($imageurl[2] >= $imageurl[1]) {
				?>
				<div class="feat-image" style="">
				<?php
				the_post_thumbnail('large', ['class' => 'feat-image__bg-image_port', 'title' => 'Feature image']);
			} ?>
			</div>
        	<?php }; ?>
     	 </div>
    <?php
  }
  echo paginate_links();
   ?>
</div>
<?php
get_footer();

?>
