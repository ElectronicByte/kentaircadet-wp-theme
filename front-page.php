<?php

get_header();
?>

<div class="hero-slider">
  <div class="hero-slider__slide" style="background-image: url(<?php echo get_theme_file_uri('/images/banner1.JPG');?>);background-position: 50% 15%;">
    <div class="hero-slider__interior container">
      <div class="hero-slider__overlay">
        <h2 class="headline headline--medium t-center">Be Part of Something Bigger</h2>
        <p class="t-center">As an Air Cadet, you are part of a team!</p>
        <p class="t-center no-margin"><a href="<?php echo home_url('/join-us'); ?>" class="btn btn--blue">Join Now</a></p>
      </div>
    </div>
  </div>
  <div class="hero-slider__slide" style="background-image: url(<?php echo get_theme_file_uri('/images/banner2.JPG');?>);">
    <div class="hero-slider__interior container">
      <div class="hero-slider__overlay">
        <h2 class="headline headline--medium t-center">Find your adventure!</h2>
        <p class="t-center">Adventure is at the very core of what it means to be an Air Cadet</p>
        <p class="t-center no-margin"><a href="<?php echo home_url('/what-we-do'); ?>" class="btn btn--blue">Learn more</a></p>
      </div>
    </div>
  </div>
  <div class="hero-slider__slide" style="background-image: url(<?php echo get_theme_file_uri('/images/banner3.JPG');?>);">
    <div class="hero-slider__interior container">
      <div class="hero-slider__overlay">
        <h2 class="headline headline--medium t-center">Achieve more as part of a Team</h2>
        <p class="t-center">As a cadet you'll solve tasks as part of a team, using everyones skills!</p>
        <p class="t-center no-margin"><a href="<?php echo home_url('/join-us'); ?>" class="btn btn--blue">Learn more</a></p>
      </div>
    </div>
  </div>
</div>
<div>
  <h2 class="headline headline--medium t-center">What Do We Do</h2>
  <p class="t-center">Find out more about what you can do in the Air Cadets</p>

  <div class="full-width-split group">
    <div class="full-width-split__one">
      <div class="full-width-split__inner">
        <i class="fas fa-fighter-jet fa-5x i-center"></i>
        <h3 class="headline headline--small t-center">Flying & Gliding</h3>
        <p class="t-center">Flying is central to your cadet experience. You’ll have loads of opportunities to take to the skies.</p>
      </div>
    </div>
    <div class="full-width-split__one">
      <div class="full-width-split__inner">
        <i class="fas fa-graduation-cap fa-5x i-center"></i>
        <h3 class="headline headline--small t-center">Earn Qualifications</h3>

        <p class="t-center">You’ll gain real qualifications that will set you apart from the crowd when you head to university or begin your chosen career.</p>
      </div>
    </div>
    <div class="full-width-split__one">
      <div class="full-width-split__inner">
        <i class="far fa-compass fa-5x i-center"></i>
        <h3 class="headline headline--small t-center">Duke of Edinburgh Award</h3>

        <p class="t-center">The DofE Award Scheme takes you through a programme of activities that will leave you a more confident, motivated and capable person.</p>
      </div>
    </div>
    <div class="full-width-split__one">
      <div class="full-width-split__inner">
        <i class="fas fa-fighter-jet fa-5x i-center"></i>
        <h3 class="headline headline--small t-center">Visit RAF Stations</h3>

        <p class="t-center">Cadets can spend a week at an RAF station where they experience the full range of life and work on it, as well as take part in sports, visits and adventure training.</p>
      </div>
    </div>
  </div>
</div>

<div class="full-width-split group">
  <div class="full-width-split__one">
    <div class="full-width-split__inner">
      <h2 class="headline headline--small-plus t-center">Highlighted Squadrons</h2>
      <?php
        $SqnSelection = new WP_Query(array(
        'posts_per_page' => -1,
        'post_type' =>'Squadron',
        'orderby'=> 'rand'
        ));
        $X = 0;
        while(($SqnSelection->have_posts()) && ($X < 3)){
          $SqnSelection->the_post(); ?>
            <div class="sqn-summary">
              <a class="sqn-summary__num t-center" href="<?php the_permalink(); ?>">
                <span class="sqn-summary__number"><?php the_field('sqn-num') ?></span>
              </a>
              <div class="sqn-summary__content">
                <h5 class="sqn-summary__title headline headline--tiny"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                <p><?php if(has_excerpt()){echo get_the_excerpt();}else{echo wp_trim_words(get_the_content(), 18);} ?> <a href="<?php the_permalink(); ?>" class="nu gray">Read more</a></p>
              </div>
            </div>
          <?php
          $X++;
        } wp_reset_postdata(); ?>

      <p class="t-center no-margin"><a href="<?php echo get_post_type_archive_link('squadron');?>" class="btn btn--blue">View All Squadrons</a></p>

    </div>
  </div>
  <div class="full-width-split__two">
    <div class="full-width-split__inner">
      <h2 class="headline headline--small-plus t-center">Latest News</h2>
      <?php
        $LatestNews = new WP_Query(array(
        'posts_per_page' => 3
        ));

        while($LatestNews->have_posts()){
          $LatestNews->the_post(); ?>
            <div class="news-summary">
              <a class="news-summary__date news-summary__date--beige t-center" href="<?php the_permalink(); ?>">
                <span class="news-summary__month"><?php the_time('M'); ?></span>
                <span class="news-summary__day"><?php the_time('d'); ?></span>
              </a>
              <div class="news-summary__content">
                <h5 class="news-summary__title headline headline--tiny"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                <p><?php if(has_excerpt()){echo get_the_excerpt();}else{echo wp_trim_words(get_the_content(), 18);} ?> <a href="<?php the_permalink(); ?>" class="nu gray">Read more</a></p>
              </div>
            </div>
          <?php
        } wp_reset_postdata(); ?>

      <p class="t-center no-margin"><a href="<?php echo site_url('/kent-news') ?>" class="btn btn--yellow">View All News Article</a></p>
    </div>
  </div>
</div>

<div class="page-sub-banner">
  <div class="page-sub-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/volunteer.JPG'); ?>);"></div>
    <div class="page-sub-banner__content container t-center c-white">
      <h1 class="headline headline--medium t-center">What can you give?</h1>
      <a href="<?php echo home_url('/join-us/volunteer/'); ?>" class="btn btn--medium btn--blue">Volunteer Now!</a>
    </div>
  </div>

<?php
get_footer();

?>
