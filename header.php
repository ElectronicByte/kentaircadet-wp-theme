<!DOCTYPE htmp>
<html <?php language_attributes(); ?>>

	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
	<div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v6.0">
	</script>
	<header class="site-header">
	  <div class="container">
      	    <div id="logo"><a class="logo-link" href="<?php echo site_url(); ?>"><img class="logo" src=" <?php echo get_theme_file_uri('/images/RAFAC-kent-WEB.png');?>"></a></div>
      	    <span class="js-search-trigger site-header__search-trigger"> <!-- <i class="fa fa-search" aria-hidden="true"> --> </i></span>
      	    <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
      	<div class="site-header__menu group">
        <nav class="main-navigation">
					<?php wp_nav_menu(array(
						'theme_location' => 'headerMenu',
					)); ?>
        </nav>
      </div>
    </div>
  </header>
	</body>

</html>
