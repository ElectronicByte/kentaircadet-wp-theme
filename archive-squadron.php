<?php

get_header();
?>

<div class="page-banner">
  <div class="page-banner__bg-image" style="background-image: url(<?php $banner_bg_image = get_field('banner_bg_image', 47); echo $banner_bg_image['sizes']['Pagebanner']; ?>);"></div>
  <div class="page-banner__content container container--narrow">
    <h1 class="page-banner__title"><?php echo get_the_title("47"); ?></h1>
    <div class="page-banner__intro">
      <p><?php the_field('banner_subtitle', 47) ?></p>
    </div>
  </div>
</div>

<div class="container container--narrow page-section">
<div id="tbscroll">
<table id="fixed">
  <tr>
  <?php
    $Sqns = new WP_Query(array(
    'posts_per_page' => -1,
    'post_type' =>'Squadron',
    'orderby'=> 'asc'
    ));

    while($Sqns->have_posts()){
      $Sqns->the_post(); ?>
        <td id="<?php the_field('sqn-num') ?>-td">
        <div class="sqn-summary">
          <a class="sqn-summary__num t-center" onclick="flyToSqn(<?php the_field('sqn-num') ?>)">
            <span class="sqn-summary__number"><?php the_field('sqn-num') ?></span>
          </a>
          <div class="sqn-summary__content">
            <h5 class="sqn-summary__title headline headline--tiny"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
            <div><?php the_field('sqn_address');?>
		<br>
	    <?php the_field('parade-nights');?></div>

            <?php
                $SocialArray=array('tel','squadron_email','squadron_website','public_fb_page','public_twitter','public_instagram');
                foreach($SocialArray as $value){
                  $sqnweb = get_field($value);
                  if(!empty($sqnweb)){
			if ($value =='squadron_website'){
				?>
				<a href="<?php echo $sqnweb ?>"><i class="fas fa-globe-europe fa-2x" aria-hidden="true"></i></a>
				<?php }
			elseif ($value == 'public_fb_page') {
                          	?>
                          	<a href="<?php echo $sqnweb ?>"><i class="fab fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                      		<?php }
                      	elseif ($value == 'public_twitter') {
                          	?>
                          	<a href="<?php echo $sqnweb ?>"><i class="fab fa-twitter-square fa-2x" aria-hidden="true"></i></a>
                          	<?php }
		      	elseif ($value == 'public_instagram') {
				?>
				<a href="<?php echo $sqnweb ?>"><i class="fab fa-instagram fa-2x" aria-hidden="true"></i></a>
				<?php }
		      	elseif ($value == 'squadron_email'){
                        	?>
                        	<a href="mailto:<?php echo $sqnweb ?>"><i class="fas fa-at fa-2x" aria-hidden="true"></i></a>
                        	<?php }

                      }
                }

                  $sqnlat = get_field('squadron_lat');
                  $sqnlong = get_field('squadron_long');
                  $sqnNum = get_field('sqn-num');
                  if(!empty($sqnlat) && !empty($sqnlong)){
                          ?>
                          <div id="<?php echo $sqnNum ?>-loc"
                            style="display: none;"
                            data-point="<?php echo $sqnlat ?>, <?php echo $sqnlong ?>">
                          </div>
                  <?php }

              ?>
          </div>
        </div>
        </td>
    <?php
    }
  echo paginate_links();
   ?>
 </tr>
</table>
</div>
<?php
  $FindSqns = new WP_Query(array(
  'p'         => 47, // ID of a page, post, or custom type
  'post_type' => 'any',
  ));

  while($FindSqns->have_posts()){
    $FindSqns->the_post();
    the_content(); ?>
      <div id="mapid"></div>
    <?php
  }; ?>


</div>

<?php
wp_enqueue_script('LeafletMap', get_theme_file_uri('/js/modules/LeafletMaps.js'));
get_footer();
?>
