<?php

get_header();

while(have_posts()) {
the_post();
?>

<div class="page-banner">
  <div class="page-banner__bg-image" style="background-image: url(<?php $banner_bg_image = get_field('banner_bg_image'); echo $banner_bg_image['sizes']['Pagebanner']; ?>);"></div>
  <div class="page-banner__content container container--narrow">
    <h1 class="page-banner__title"><?php echo the_title(); ?></h1>
    <div class="page-banner__intro">
      <p><?php the_field('banner_subtitle') ?></p>
    </div>
  </div>
</div>

<div class="container container--narrow page-section">

<?php
$ParentID = wp_get_post_parent_id(get_the_ID());
if($ParentID){ ?>
  <div class="metabox metabox--position-up metabox--with-home-link">
    <p><a class="metabox__blog-home-link" href="<?php echo get_permalink($ParentID) ?>"><i class="fas fa-home" aria-hidden="true"></i> Back to <?php echo get_the_title($ParentID); ?></a> <span class="metabox__main"><?php the_title(); ?></span></p>
  </div>
<?php
}
?>

<?php
$ParentTest = get_pages(array(
  'child_of'=> get_the_ID()
));
if($ParentID or $ParentTest) { ?>
<div class="page-links">
    <h2 class="page-links__title"><a href="<?php echo get_permalink($ParentID); ?>"><?php echo get_the_title($ParentID); ?></a></h2>
    <ul class="min-list">
      <?php
      if($ParentID){
        $ChildrenOf = $ParentID;
      } else {
        $ChildrenOf = get_the_ID();
      }
        wp_list_pages(array(
          'title_li' => NULL,
          'child_of' => $ChildrenOf,
          'sort_column'  => 'menu_order',
        ));
        ?>
    </ul>
  </div>
<?php } ?>

  <div class="generic-content">
    <?php the_content(); ?>
  </div>

</div>

<?php

}
get_footer();

?>
