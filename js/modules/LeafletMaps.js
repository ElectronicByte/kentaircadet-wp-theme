var base_url = window.location.origin;

var RAFroundle = L.icon({
    iconUrl: base_url+'/wp-content/themes/kentaircadet-wp-theme/images/RAF_roundel_80px.png',
    iconSize:     [60, 60], // size of the icon
    iconAnchor:   [24, 51], // point of the icon which will correspond to marker's location
    popupAnchor:  [-0, -50] // point from which the popup should open relative to the iconAnchor
});


var WingMap = L.map('mapid').setView([51.2214,0.5524], 9);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
	minZoom: 8,
        attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
      }).addTo(WingMap);


// show a marker on the map

var locations = ["173", "228", "358", "359", "402", "593", "1051", "1227", "1579", "1903", "2427", "25", "40F", "129", "213", "1039", "1404", "2158", "2230", "2316", "2374", "2520", "99", "305", "312", "354", "438", "500", "1063", "1242", "2235", "2433", "2513"];
var i;

for (i = 0; i < locations.length; i++) { 
  	var latlong = document.getElementById(locations[i]+"-loc");
  	if(latlong){
    		var divDataPoint = latlong.getAttribute('data-point');
		var sqnLatLong = divDataPoint.split(', ');
		var marker = L.marker([sqnLatLong[0], sqnLatLong[1]], {icon: RAFroundle}).addTo(WingMap).on('click', tdScroll);
		marker.bindPopup("<b>"+locations[i]+" Squadron</b>");    
	}
}

function tdScroll(e) {
	var SqnNum = e.target._popup._content;
	Sqn = SqnNum.substring(3, SqnNum.indexOf(' '));
	var elem = document.getElementById(Sqn + "-td");
	elem.scrollIntoView({behavior: "smooth", block: "nearest", inline: "center"});
}


function flyToSqn(SqnNum) {
  var latlong = document.getElementById(SqnNum+"-loc");
  if(latlong){
      console.log("latlong is valid");
    var divDataPoint = latlong.getAttribute('data-point');
    var sqnLatLong = divDataPoint.split(', ');
    var zoom = 14;
    WingMap.flyTo([sqnLatLong[0], sqnLatLong[1]], zoom);
  }
}

/*var WingHQmarker = L.marker([51.284655, 0.527109]).addTo(WingMap);
WingHQmarker.bindPopup("<b>Wing Headquarters</b>").openPopup();

var val = "<?php echo $val ?>"; */
